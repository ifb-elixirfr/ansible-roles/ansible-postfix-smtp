---

- name: Update repositories cache
  apt:
    update_cache: true
  when: ansible_os_family == 'Debian'

# Maybe we should enable test only on host where postfix have to be installed
- name: Ensure postfix is installed.
  package:
    name: postfix
    state: present

- name: configure postfix inet_interfaces
  replace:
    path: "{{ postfix_configfile }}"
    regexp: '^inet_interfaces = .*$'
    replace: "inet_interfaces = {{ postfix_inet_interfaces }}"
  when: postfix_inet_interfaces is defined
  notify: restart postfix

- name: configure postfix relayhost
  lineinfile:
    path: "{{ postfix_configfile }}"
    regexp: 'relayhost = .*'
    insertafter: '^#relayhost = \[an\.ip\.add\.ress\]'
    line: "relayhost = {{ smtp_server }}"
  when: smtp_server is defined
  notify: restart postfix

# Sending mail
# Use @domain insteadof @hostname
# smtp_use_domain: (true|false)
# Disadvantage: we lost the host information (sender)
- name: use or not domain for sending mail (@domain)
  replace:
    path: "{{ postfix_configfile }}"
    regexp: '^[#]*myorigin = \$mydomain$'
    replace: "{{ smtp_use_domain | ternary('','#') }}myorigin = $mydomain"
  when: smtp_use_domain is defined
  notify: restart postfix

# Rewrite sender address "user.hostname@domain"
#
# Exemple en utilisant le domaine "france-bioinformatique.fr":
# sender: root@slurm-client.ifb.local
#         --> root.slurm-client@france-bioinformatique.fr
# Documentation: https://www.unix-experience.fr/postfix/rewrite_sender_dest/
# Pour tester le filtre postmap:
# postmap -q "root@<hostname>" regexp:/etc/postfix/sender_canonical
- name: rewrite sender address (user.hostname@domain)
  copy:
    content: |
      if ! /@{{ smtp_rewrite_sender_domain  }}$/
      /^(.*)@([^\.]+)/               ${1}.${2}@{{ smtp_rewrite_sender_domain }}
      endif
    dest: "{{ postfix_configpath }}/sender_canonical"
  when: smtp_rewrite_sender_domain is defined
  register: postfix_sender_canonical

- name: reload postmap  # noqa 503
  command: postmap "{{ postfix_configpath }}/sender_canonical"
  notify: restart postfix
  when: postfix_sender_canonical.changed

- name: add rewrite sender to posfix configuration
  blockinfile:
    path: "{{ postfix_configfile }}"
    # yamllint disable-line rule:line-length
    block: "sender_canonical_maps = regexp:{{ postfix_configpath }}/sender_canonical"
  when: smtp_rewrite_sender_domain is defined
  notify: restart postfix
