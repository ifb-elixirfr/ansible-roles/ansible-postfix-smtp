# postfix-smtp configuration

**Usefull when `smtp_server` accept only incoming email from well-known domain**.

Configure `postfix`:
- `postfix_configpath`: postfix directory configuration path (default /etc/postfix)
- `postfix_configfile`: postfix main configuration file (default /etc/postfix/main.cf)
- `postfix_inet_interfaces`: The inet_interfaces parameter specifies the network interface addresses that this mail system receives mail on. 
- `smtp_server`: Configure `relay_host`
- `smtp_use_domain`: Send email using domain instead of FQDN (default `false`). Sender adress will be: <user>@<domain> (instead of <user>@<hostname>)
- `smtp_rewrite_sender_domain`: Send email using format <user>.<hostname>@<domain> and domain defined

Exemple:
```
# SMTP relay host
smtp_server: "192.168.103.247"
smtp_rewrite_sender_domain: 'france-bioinformatique.fr'

```
